package com.adrianhescalante.githubsearch.usecases;

import com.adrianhescalante.common.database.AppDatabase;
import com.adrianhescalante.common.models.BaseItemModel;
import com.adrianhescalante.common.network.BaseClient;
import com.adrianhescalante.common.usecases.BaseUseCase;
import com.adrianhescalante.common.usecases.Executor;
import com.adrianhescalante.common.usecases.OnResponseListener;
import com.adrianhescalante.common.usecases.ResponseNotifier;
import com.adrianhescalante.common.usecases.visited.VisitedUseCase;
import com.adrianhescalante.githubsearch.models.User;
import com.adrianhescalante.githubsearch.network.GithubApi;
import com.adrianhescalante.githubsearch.network.SearchUsersDto;

import java.io.IOException;
import java.util.List;

import retrofit2.Response;

public class SearchUsersUseCase extends BaseUseCase {
    private BaseClient<GithubApi> client;
    private VisitedUseCase visitedUseCase;

    public SearchUsersUseCase(Executor executor, BaseClient<GithubApi> client, AppDatabase database) {
        super(executor);

        this.client = client;
        this.visitedUseCase = new VisitedUseCase(executor, database);
    }

    public void searchUsers(final String query, final OnResponseListener<List<User>> listener) {
        executor.runOnBackground(new Runnable() {
            @Override
            public void run() {
                ResponseNotifier<List<User>> notifier = new ResponseNotifier<>(executor, listener);

                final Response<SearchUsersDto> usersResponse;
                try {
                    usersResponse = client.getApi().searchUsers(query).execute();
                    if (usersResponse.isSuccessful()) {
                        List<User> users = User.fromSearchUsersDto(usersResponse.body());

                        List<BaseItemModel> baseItemModels = visitedUseCase.getAllVisitedSync();
                        notifier.notifySuccess(visitedUseCase.mergeItems(baseItemModels, users));
                    } else {
                        notifier.notifyError(usersResponse.message());
                    }
                } catch (IOException e) {
                    notifier.notifyError(e);
                }
            }
        });
    }
}
