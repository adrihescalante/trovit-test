package com.adrianhescalante.githubsearch.network;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SearchUsersDto {
    @SerializedName("items")
    public List<SearchUserDto> items;

    public static class SearchUserDto {
        @SerializedName("id")
        public int id;
        @SerializedName("login")
        public String login;
        @SerializedName("avatar_url")
        public String avatarUrl;
        @SerializedName("html_url")
        public String htmlUrl;
    }
}
