package com.adrianhescalante.githubsearch.network;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface GithubApi {
    @GET("search/users")
    Call<SearchUsersDto> searchUsers(@Query("q") String query);
}
