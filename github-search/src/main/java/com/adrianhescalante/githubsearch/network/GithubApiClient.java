package com.adrianhescalante.githubsearch.network;

import com.adrianhescalante.common.network.BaseClient;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class GithubApiClient implements BaseClient<GithubApi> {
    private static final String BASE_URL = "https://api.github.com/";

    private GithubApi client;

    public GithubApiClient() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        client = retrofit.create(GithubApi.class);
    }

    @Override
    public GithubApi getApi() {
        return client;
    }
}
