package com.adrianhescalante.githubsearch.ui.search;

import android.arch.persistence.room.Room;

import com.adrianhescalante.common.BuildConfig;
import com.adrianhescalante.common.database.AppDatabase;
import com.adrianhescalante.common.ui.detail.DetailActivity;
import com.adrianhescalante.common.ui.search.BaseSearchActivity;
import com.adrianhescalante.common.ui.search.SearchMvp;
import com.adrianhescalante.common.usecases.ExecutorSingleBackgroundThread;
import com.adrianhescalante.githubsearch.models.User;
import com.adrianhescalante.githubsearch.network.GithubApiClient;

public class GithubSearchActivity extends BaseSearchActivity<User> {

    @Override
    protected SearchMvp.Presenter<User> providePresenter() {
        AppDatabase database = Room.databaseBuilder(this, AppDatabase.class, BuildConfig.DB_NAME).build();
        return new SearchPresenter(this, new ExecutorSingleBackgroundThread(), new GithubApiClient(), database);
    }

    @Override
    public void showDetail(User searchItem) {
        DetailActivity.startActivity(this, searchItem);
    }
}
