package com.adrianhescalante.githubsearch.ui.search;

import com.adrianhescalante.common.database.AppDatabase;
import com.adrianhescalante.common.network.BaseClient;
import com.adrianhescalante.common.ui.search.SearchMvp;
import com.adrianhescalante.common.usecases.Error;
import com.adrianhescalante.common.usecases.Executor;
import com.adrianhescalante.common.usecases.OnResponseListener;
import com.adrianhescalante.githubsearch.models.User;
import com.adrianhescalante.githubsearch.network.GithubApi;
import com.adrianhescalante.githubsearch.usecases.SearchUsersUseCase;

import java.util.List;

public class SearchPresenter extends com.adrianhescalante.common.ui.search.SearchPresenter<User> {
    private SearchUsersUseCase searchUsersUseCase;

    SearchPresenter(SearchMvp.View<User> viewTranslator, Executor executor, BaseClient<GithubApi> githubApiClient, AppDatabase database) {
        super(viewTranslator, executor, database);
        this.searchUsersUseCase = new SearchUsersUseCase(executor, githubApiClient, database);
    }

    @Override
    protected void search(String query) {
        viewTranslator.showLoading();
        searchUsersUseCase.searchUsers(query, new OnResponseListener<List<User>>() {
            @Override
            public void onSuccess(List<User> users) {
                if (users.size() > 0) {
                    viewTranslator.showUsers(users);
                } else {
                    viewTranslator.showNoResults();
                }
            }

            @Override
            public void onError(Error error) {
                viewTranslator.showNoResults();
            }
        });
    }
}
