package com.adrianhescalante.githubsearch.models;

import com.adrianhescalante.common.models.BaseItemModel;
import com.adrianhescalante.common.ui.detail.DetailMvp;
import com.adrianhescalante.common.ui.search.SearchMvp;
import com.adrianhescalante.githubsearch.network.SearchUsersDto;

import java.util.ArrayList;
import java.util.List;

public class User extends BaseItemModel implements SearchMvp.Model, DetailMvp.Model {
    private int id;
    private String name;
    private String imageUrl;
    private String webUrl;

    public User(int id) {
        super(id);

        this.id = id;
    }

    public static List<User> fromSearchUsersDto(SearchUsersDto dtos) {
        List<User> users = new ArrayList<>();

        for (SearchUsersDto.SearchUserDto dto : dtos.items) {
            users.add(User.fromSearchUserDto(dto));
        }

        return users;
    }

    public static User fromSearchUserDto(SearchUsersDto.SearchUserDto dto) {
        User user = new User(dto.id);

        user.name = dto.login;
        user.imageUrl = dto.avatarUrl;
        user.webUrl = dto.htmlUrl;

        return user;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public String getTitle() {
        return name;
    }

    @Override
    public String getImageUrl() {
        return imageUrl;
    }

    public String getProfileUrl() {
        return webUrl;
    }
}
