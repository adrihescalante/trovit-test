package com.adrianhescalante.common.usecases;

public class Error {
    private String message;
    private Throwable throwable;

    public Error() {

    }

    public Error(String message) {
        this.message = message;
    }

    public Error(Exception exception) {
        this.message = exception.getMessage();
        this.throwable = exception;
    }

    public String getMessage() {
        return message;
    }

    public Throwable getThrowable() {
        return throwable;
    }
}
