package com.adrianhescalante.common.usecases;

public class ResponseNotifier<T> {
    private Executor executor;
    private OnResponseListener<T> listener;

    public ResponseNotifier(Executor executor, OnResponseListener<T> listener) {
        this.executor = executor;
        this.listener = listener;
    }

    public void notifySuccess(T success) {
        notifyResponse(success, null);
    }

    public void notifyError(String errorMessage) {
        Error error = new Error(errorMessage);
        notifyResponse(null, error);
    }

    public void notifyError(Exception exception) {
        Error error = new Error();
        notifyResponse(null, error);
    }

    private void notifyResponse(final T success, final Error error) {
        executor.runOnForeground(new Runnable() {
            @Override
            public void run() {
                if (listener != null) {
                    if (success != null) {
                        listener.onSuccess(success);
                    } else {
                        listener.onError(error);
                    }
                }
            }
        });
    }
}
