package com.adrianhescalante.common.usecases;

import com.adrianhescalante.common.models.BaseItemModel;

import java.util.List;

public abstract class BaseItemUseCase extends BaseUseCase {
    public BaseItemUseCase(Executor executor) {
        super(executor);
    }

    public <T extends BaseItemModel> List<T> mergeItems(List<? extends BaseItemModel> origin, List<T> destination) {
        for (BaseItemModel originModel : origin) {
            for (BaseItemModel destModel : destination) {
                if (destModel.getDbId().equals(originModel.getDbId())) {
                    destModel.setVisited(originModel.isVisited());
                    destModel.setFavorite(originModel.isFavorite());
                }
            }
        }

        return destination;
    }
}
