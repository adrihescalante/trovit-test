package com.adrianhescalante.common.usecases;

public class BaseUseCase {
    protected Executor executor;

    public BaseUseCase(Executor executor) {
        this.executor = executor;
    }
}
