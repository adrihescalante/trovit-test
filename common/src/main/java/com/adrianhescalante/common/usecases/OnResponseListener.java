package com.adrianhescalante.common.usecases;

public interface OnResponseListener<T> {
    void onSuccess(T response);
    void onError(Error error);
}
