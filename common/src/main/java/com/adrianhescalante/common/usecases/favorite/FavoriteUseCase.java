package com.adrianhescalante.common.usecases.favorite;

import com.adrianhescalante.common.database.AppDatabase;
import com.adrianhescalante.common.database.BaseItemDao;
import com.adrianhescalante.common.models.BaseItemModel;
import com.adrianhescalante.common.usecases.BaseItemUseCase;
import com.adrianhescalante.common.usecases.Executor;
import com.adrianhescalante.common.usecases.OnResponseListener;
import com.adrianhescalante.common.usecases.ResponseNotifier;

public class FavoriteUseCase extends BaseItemUseCase {
    private AppDatabase database;

    public FavoriteUseCase(Executor executor, AppDatabase database) {
        super(executor);

        this.database = database;
    }

    public void saveAsFavorite(final BaseItemModel model, final OnResponseListener<BaseItemModel> listener) {
        model.setFavorite(true);
        saveBaseItemModel(model, listener);
    }

    public void saveAsNonFavorite(final BaseItemModel model, final OnResponseListener<BaseItemModel> listener) {
        model.setFavorite(false);
        saveBaseItemModel(model, listener);
    }

    private void saveBaseItemModel(final BaseItemModel model, final OnResponseListener<BaseItemModel> listener) {
        final ResponseNotifier<BaseItemModel> notifier = new ResponseNotifier<>(executor, listener);

        executor.runOnBackground(new Runnable() {
            @Override
            public void run() {
                BaseItemDao dao = database.baseItemDaoAccess();
                dao.upsertBaseItem(model);

                notifier.notifySuccess(model);
            }
        });
    }
}
