package com.adrianhescalante.common.usecases;

import android.os.Handler;
import android.os.Looper;

import java.util.concurrent.Executors;

public class ExecutorSingleBackgroundThread implements Executor {
    private java.util.concurrent.Executor backgroundExecutor = Executors.newSingleThreadExecutor();
    private Handler foregroundHandler = new Handler(Looper.getMainLooper());

    @Override
    public void runOnForeground(Runnable runnable) {
        foregroundHandler.post(runnable);
    }

    public void runOnBackground(Runnable runnable) {
        backgroundExecutor.execute(runnable);
    }
}
