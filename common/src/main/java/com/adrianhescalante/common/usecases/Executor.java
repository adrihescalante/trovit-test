package com.adrianhescalante.common.usecases;

public interface Executor {
    void runOnForeground(Runnable runnable);

    void runOnBackground(Runnable runnable);
}
