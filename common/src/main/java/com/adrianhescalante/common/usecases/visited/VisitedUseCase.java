package com.adrianhescalante.common.usecases.visited;

import android.support.annotation.WorkerThread;

import com.adrianhescalante.common.database.AppDatabase;
import com.adrianhescalante.common.models.BaseItemModel;
import com.adrianhescalante.common.usecases.BaseItemUseCase;
import com.adrianhescalante.common.usecases.Executor;
import com.adrianhescalante.common.usecases.OnResponseListener;
import com.adrianhescalante.common.usecases.ResponseNotifier;

import java.util.List;

public class VisitedUseCase extends BaseItemUseCase {
    private AppDatabase database;

    public VisitedUseCase(Executor executor, AppDatabase database) {
        super(executor);

        this.database = database;
    }

    @WorkerThread
    public List<BaseItemModel> getAllVisitedSync() {
        return database.baseItemDaoAccess().getAll();
    }

    public void saveAsVisited(final BaseItemModel baseItemModel, OnResponseListener<BaseItemModel> listener) {
        final ResponseNotifier<BaseItemModel> notifier = new ResponseNotifier<>(executor, listener);

        executor.runOnBackground(new Runnable() {
            @Override
            public void run() {
                baseItemModel.setVisited(true);
                database.baseItemDaoAccess().upsertBaseItem(baseItemModel);
                notifier.notifySuccess(baseItemModel);
            }
        });
    }
}
