package com.adrianhescalante.common.network;

public interface BaseClient<T> {
    T getApi();
}
