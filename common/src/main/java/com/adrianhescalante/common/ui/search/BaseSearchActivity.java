package com.adrianhescalante.common.ui.search;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.adrianhescalante.common.R;
import com.adrianhescalante.common.ui.BaseActivity;
import com.adrianhescalante.common.ui.ItemAdapter;

import java.util.List;

public abstract class BaseSearchActivity<T extends SearchMvp.Model> extends BaseActivity implements SearchMvp.View<T> {
    private SearchMvp.Presenter<T> presenter;

    private RecyclerView recyclerView;
    private ProgressBar progressView;
    private TextView messageView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initViews();

        presenter = providePresenter();
    }

    protected abstract SearchMvp.Presenter<T> providePresenter();

    private void initViews() {
        setContentView(R.layout.activity_search);

        final SearchView searchView = findViewById(R.id.activity_search_user_search);
        recyclerView = findViewById(R.id.activity_search_user_list);
        progressView = findViewById(R.id.activity_search_user_progress);
        messageView = findViewById(R.id.activity_search_user_message);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                presenter.onSearchTextSubmitted(s);
                searchView.clearFocus();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        presenter.onResume();
    }

    @Override
    public void showUsers(List<T> usersList) {
        recyclerView.setAdapter(new ItemAdapter<>(usersList, presenter));

        progressView.setVisibility(View.GONE);
        messageView.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showNoResults() {
        showMessage(getString(R.string.activity_search_no_results));
    }

    @Override
    public void showLoading() {
        progressView.setVisibility(View.VISIBLE);
        messageView.setVisibility(View.GONE);
        recyclerView.setVisibility(View.GONE);
    }

    @Override
    public void showError(String message) {
        showMessage(message);
    }

    protected void showMessage(String message) {
        messageView.setText(message);

        progressView.setVisibility(View.GONE);
        messageView.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
    }
}
