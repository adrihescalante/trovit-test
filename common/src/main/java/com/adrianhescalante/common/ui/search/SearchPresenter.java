package com.adrianhescalante.common.ui.search;

import com.adrianhescalante.common.database.AppDatabase;
import com.adrianhescalante.common.models.BaseItemModel;
import com.adrianhescalante.common.ui.ItemAdapter;
import com.adrianhescalante.common.usecases.Executor;
import com.adrianhescalante.common.usecases.visited.VisitedUseCase;

public abstract class SearchPresenter<T extends BaseItemModel & SearchMvp.Model> implements SearchMvp.Presenter<T>, ItemAdapter.OnItemClickListener<T> {
    protected SearchMvp.View<T> viewTranslator;
    private VisitedUseCase visitedUseCase;

    private String lastQuery;

    public SearchPresenter(SearchMvp.View<T> viewTranslator, Executor executor, AppDatabase database) {
        this.viewTranslator = viewTranslator;
        this.visitedUseCase = new VisitedUseCase(executor, database);
    }

    public void onSearchTextSubmitted(String query) {
        this.lastQuery = query;
        search(query);
    }

    protected abstract void search(String query);

    @Override
    public void onItemClick(T item) {
        visitedUseCase.saveAsVisited(item, null);
        viewTranslator.showDetail(item);
    }

    public void onResume() {
        if (lastQuery != null) {
            search(lastQuery);
        }
    }
}
