package com.adrianhescalante.common.ui.search;

import com.adrianhescalante.common.ui.ItemAdapter;

import java.util.List;

public interface SearchMvp {
    interface Model {
        int getId();

        String getTitle();

        String getImageUrl();

        boolean isFavorite();

        boolean isVisited();
    }

    interface View<T extends SearchMvp.Model> {
        void showUsers(List<T> searchResults);

        void showNoResults();

        void showLoading();

        void showDetail(T searchItem);

        void showError(String message);
    }

    interface Presenter<T extends SearchMvp.Model> extends ItemAdapter.OnItemClickListener<T> {
        void onSearchTextSubmitted(String query);

        void onResume();
    }
}
