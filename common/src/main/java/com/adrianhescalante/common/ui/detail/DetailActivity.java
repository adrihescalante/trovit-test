package com.adrianhescalante.common.ui.detail;

import android.annotation.SuppressLint;
import android.arch.persistence.room.Room;
import android.content.Intent;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.Window;
import android.widget.ImageView;

import com.adrianhescalante.common.BuildConfig;
import com.adrianhescalante.common.R;
import com.adrianhescalante.common.database.AppDatabase;
import com.adrianhescalante.common.models.BaseItemModel;
import com.adrianhescalante.common.ui.BaseActivity;
import com.adrianhescalante.common.usecases.ExecutorSingleBackgroundThread;
import com.squareup.picasso.Picasso;

public class DetailActivity<T extends BaseItemModel & DetailMvp.Model> extends BaseActivity implements DetailMvp.View {
    public static final String EXTRA_DETAIL_ITEM = "ExtraDetailItem";

    private T detailItem;

    private DetailPresenter presenter;

    public static <T extends BaseItemModel & DetailMvp.Model> void startActivity(BaseActivity activity, T detailItem) {
        Intent intent = new Intent(activity, DetailActivity.class);
        intent.putExtra(EXTRA_DETAIL_ITEM, detailItem);

        activity.startActivity(intent);
    }

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);

        setContentView(R.layout.activity_detail);

        detailItem = (T) getIntent().getExtras().getSerializable(EXTRA_DETAIL_ITEM);
        if (detailItem == null) {
            throw new IllegalArgumentException("Missing DetailItem argument");
        }

        initViews();

        AppDatabase database = Room.databaseBuilder(getApplicationContext(), AppDatabase.class, BuildConfig.DB_NAME).build();
        presenter = new DetailPresenter(this, new ExecutorSingleBackgroundThread(), database, detailItem);
    }

    private void initViews() {
        setSupportActionBar((Toolbar) findViewById(R.id.activity_detail_toolbar));
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(detailItem.getTitle());
        }

        Picasso.with(this)
                .load(detailItem.getImageUrl())
                .into((ImageView) findViewById(R.id.activity_detail_image));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        new MenuInflater(this).inflate(R.menu.activity_detail, menu);

        updateFavoriteMenuOption(menu);
        tintOptionsWithThemeColor(menu);

        return true;
    }

    private void updateFavoriteMenuOption(Menu menu) {
        MenuItem menuItem = menu.findItem(R.id.action_favorite);
        int iconResource;
        String actionText;

        if (detailItem.isFavorite()) {
            iconResource = R.drawable.ic_favorite_white_24dp;
            actionText = getString(R.string.activity_detail_action_remove_favorite);
        } else {
            iconResource = R.drawable.ic_favorite_border_white_24dp;
            actionText = getString(R.string.activity_detail_action_add_favorite);
        }
        menuItem.setIcon(iconResource);
        menuItem.setTitle(actionText);
    }

    private void tintOptionsWithThemeColor(Menu menu) {
        TypedValue typedValue = new TypedValue();
        Resources.Theme theme = getTheme();
        theme.resolveAttribute(android.R.attr.textColorPrimary, typedValue, true);
        TypedArray arr = obtainStyledAttributes(typedValue.data, new int[]{android.R.attr.textColorPrimary});
        int textColorPrimary = arr.getColor(0, -1);
        arr.recycle();

        for (int i = 0; i < menu.size(); i++) {
            MenuItem item = menu.getItem(i);
            Drawable drawable = item.getIcon();
            drawable.setTint(textColorPrimary);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == R.id.action_profile) {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(detailItem.getProfileUrl()));
            startActivity(browserIntent);

            return true;
        } else if (itemId == R.id.action_favorite) {
            presenter.onFavoriteClick();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    @Override
    public void updateFavorite() {
        invalidateOptionsMenu();
    }
}
