package com.adrianhescalante.common.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.adrianhescalante.common.R;
import com.adrianhescalante.common.ui.search.SearchMvp;
import com.squareup.picasso.Picasso;

public class ItemRowView extends LinearLayout {
    private ImageView imageView;
    private TextView bestMatch;
    private TextView nameView;
    private ImageView favoriteView;

    public ItemRowView(Context context) {
        super(context);
        init(context);
    }

    public ItemRowView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public ItemRowView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        inflate(context, R.layout.row_item, this);

        setOrientation(HORIZONTAL);
        setGravity(Gravity.CENTER_VERTICAL);
        int padding = context.getResources().getDimensionPixelSize(R.dimen.default_side_space);
        setPadding(padding, padding, padding, padding);

        imageView = findViewById(R.id.row_item_image);
        bestMatch = findViewById(R.id.row_item_best_match);
        nameView = findViewById(R.id.row_item_name);
        favoriteView = findViewById(R.id.row_item_favorite);
    }

    public void setSearchModel(SearchMvp.Model model, boolean isBestMatch) {
        int colorRes = model.isVisited() ? R.color.row_visited : android.R.color.transparent;
        setBackgroundResource(colorRes);

        int favVisibility = model.isFavorite() ? View.VISIBLE : View.GONE;
        favoriteView.setVisibility(favVisibility);

        if (model.getImageUrl() != null) {
            Picasso.with(getContext())
                    .load(model.getImageUrl())
                    .into(imageView);
            imageView.setVisibility(View.VISIBLE);
        } else {
            imageView.setVisibility(View.GONE);
        }

        bestMatch.setVisibility(isBestMatch ? View.VISIBLE : View.GONE);

        nameView.setText(model.getTitle());
    }
}
