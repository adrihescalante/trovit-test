package com.adrianhescalante.common.ui.detail;

public interface DetailMvp {
    interface Model {
        String getTitle();

        String getImageUrl();

        String getProfileUrl();
    }

    interface View {
        void updateFavorite();
    }

    interface Presenter {
        void onFavoriteClick();
    }
}
