package com.adrianhescalante.common.ui.detail;

import com.adrianhescalante.common.database.AppDatabase;
import com.adrianhescalante.common.models.BaseItemModel;
import com.adrianhescalante.common.usecases.Error;
import com.adrianhescalante.common.usecases.Executor;
import com.adrianhescalante.common.usecases.OnResponseListener;
import com.adrianhescalante.common.usecases.favorite.FavoriteUseCase;

public class DetailPresenter implements DetailMvp.Presenter {
    private DetailMvp.View view;
    private FavoriteUseCase favoriteUseCase;

    private BaseItemModel model;
    private OnResponseListener<BaseItemModel> onFavoriteResponse = new OnResponseListener<BaseItemModel>() {
        @Override
        public void onSuccess(BaseItemModel response) {
            view.updateFavorite();
        }

        @Override
        public void onError(Error error) {
            view.updateFavorite();
        }
    };

    public DetailPresenter(DetailMvp.View view, Executor executor, AppDatabase database, BaseItemModel model) {
        this.view = view;
        favoriteUseCase = new FavoriteUseCase(executor, database);

        this.model = model;
    }

    @Override
    public void onFavoriteClick() {
        if (model.isFavorite()) {
            favoriteUseCase.saveAsNonFavorite(model, onFavoriteResponse);
        } else {
            favoriteUseCase.saveAsFavorite(model, onFavoriteResponse);
        }
    }
}
