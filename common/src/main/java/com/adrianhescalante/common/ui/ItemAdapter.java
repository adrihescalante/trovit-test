package com.adrianhescalante.common.ui;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.adrianhescalante.common.ui.search.SearchMvp;

import java.util.ArrayList;
import java.util.List;

public class ItemAdapter<T extends SearchMvp.Model> extends RecyclerView.Adapter<ItemAdapter.ItemViewHolder<T>> {
    private List<T> items = new ArrayList<>();
    private OnItemClickListener<T> listener;

    public ItemAdapter(List<T> items, OnItemClickListener<T> listener) {
        this.items.addAll(items);
        this.listener = listener;

        setHasStableIds(true);
    }

    @Override
    public ItemViewHolder<T> onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemRowView itemView = new ItemRowView(parent.getContext());
        itemView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        final ItemViewHolder<T> holder = new ItemViewHolder<>(itemView);

        if (listener != null) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onItemClick(items.get(holder.getAdapterPosition()));
                }
            });
        }

        return holder;
    }

    @Override
    public void onBindViewHolder(ItemViewHolder<T> holder, int position) {
        holder.onBind(items.get(position), position == 0);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public long getItemId(int position) {
        return items.get(position).getId();
    }

    static class ItemViewHolder<T extends SearchMvp.Model> extends RecyclerView.ViewHolder {
        ItemViewHolder(ItemRowView itemView) {
            super(itemView);
        }

        void onBind(T item, boolean isFirst) {
            ((ItemRowView) itemView).setSearchModel(item, isFirst);
        }
    }

    public interface OnItemClickListener<T extends SearchMvp.Model> {
        void onItemClick(T item);
    }
}
