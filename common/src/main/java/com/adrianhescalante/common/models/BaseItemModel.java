package com.adrianhescalante.common.models;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import java.io.Serializable;

@Entity(tableName = "base_item")
public class BaseItemModel extends BaseModel implements Serializable {
    public static final String TABLE_NAME = "base_item";

    public BaseItemModel() {

    }

    public BaseItemModel(int id) {
        this.dbId = generateDbId(id);
    }

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "dbId")
    private String dbId;
    @ColumnInfo(name = "is_favorite")
    private boolean isFavorite;
    @ColumnInfo(name = "is_visited")
    private boolean isVisited;

    private String generateDbId(int id) {
        return getClass().getSimpleName() + "|" + id;
    }

    @NonNull
    public String getDbId() {
        return dbId;
    }

    public void setDbId(@NonNull String dbId) {
        this.dbId = dbId;
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public void setFavorite(boolean favorite) {
        isFavorite = favorite;
    }

    public boolean isVisited() {
        return isVisited;
    }

    public void setVisited(boolean visited) {
        isVisited = visited;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BaseItemModel that = (BaseItemModel) o;

        return dbId.equals(that.dbId);
    }

    @Override
    public int hashCode() {
        return dbId.hashCode();
    }
}
