package com.adrianhescalante.common.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.adrianhescalante.common.models.BaseItemModel;

@Database(entities = {BaseItemModel.class}, version = 1, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {
    public abstract BaseItemDao baseItemDaoAccess();
}
