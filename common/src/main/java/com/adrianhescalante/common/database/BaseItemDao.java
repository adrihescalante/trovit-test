package com.adrianhescalante.common.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.adrianhescalante.common.models.BaseItemModel;

import java.util.List;

@Dao
public interface BaseItemDao {
    @Query("SELECT * FROM " + BaseItemModel.TABLE_NAME)
    List<BaseItemModel> getAll();

    @Query("SELECT * FROM " + BaseItemModel.TABLE_NAME + " WHERE is_favorite = 1")
    List<BaseItemModel> getAllFavorites();

    @Query("SELECT * FROM " + BaseItemModel.TABLE_NAME + " WHERE is_visited = 1")
    List<BaseItemModel> getAllVisited();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void upsertBaseItem(BaseItemModel baseItemModel);
}
