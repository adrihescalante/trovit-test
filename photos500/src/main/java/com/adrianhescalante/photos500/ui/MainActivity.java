package com.adrianhescalante.photos500.ui;

import android.arch.persistence.room.Room;

import com.adrianhescalante.common.BuildConfig;
import com.adrianhescalante.common.database.AppDatabase;
import com.adrianhescalante.common.ui.detail.DetailActivity;
import com.adrianhescalante.common.ui.search.BaseSearchActivity;
import com.adrianhescalante.common.ui.search.SearchMvp;
import com.adrianhescalante.common.usecases.ExecutorSingleBackgroundThread;
import com.adrianhescalante.photos500.models.Photo;

public class MainActivity extends BaseSearchActivity<Photo> {

    @Override
    public void showDetail(Photo photo) {
        DetailActivity.startActivity(this, photo);
    }

    @Override
    protected SearchMvp.Presenter<Photo> providePresenter() {
        AppDatabase database = Room.databaseBuilder(this, AppDatabase.class, BuildConfig.DB_NAME).build();
        return new MainPresenter(this, new ExecutorSingleBackgroundThread(), database);
    }
}
