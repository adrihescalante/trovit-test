package com.adrianhescalante.photos500.ui;

import com.adrianhescalante.common.database.AppDatabase;
import com.adrianhescalante.common.ui.search.SearchMvp;
import com.adrianhescalante.common.ui.search.SearchPresenter;
import com.adrianhescalante.common.usecases.Error;
import com.adrianhescalante.common.usecases.Executor;
import com.adrianhescalante.common.usecases.OnResponseListener;
import com.adrianhescalante.photos500.models.Photo;
import com.adrianhescalante.photos500.network.Photos500Client;
import com.adrianhescalante.photos500.usecases.SearchPhotosUseCase;

import java.util.List;

public class MainPresenter extends SearchPresenter<Photo> {
    private SearchPhotosUseCase searchPhotosUseCase;

    MainPresenter(SearchMvp.View<Photo> view, Executor executor, AppDatabase database) {
        super(view, executor, database);
        this.searchPhotosUseCase = new SearchPhotosUseCase(executor, new Photos500Client(), database);
    }

    @Override
    protected void search(String query) {
        viewTranslator.showLoading();
        searchPhotosUseCase.searchPhotos(query, new OnResponseListener<List<Photo>>() {
            @Override
            public void onSuccess(List<Photo> response) {
                if (!response.isEmpty()) {
                    viewTranslator.showUsers(response);
                } else {
                    viewTranslator.showNoResults();
                }
            }

            @Override
            public void onError(Error error) {
                viewTranslator.showError(error.getMessage());
            }
        });
    }
}
