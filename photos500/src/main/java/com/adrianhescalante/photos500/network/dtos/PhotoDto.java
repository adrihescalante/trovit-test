package com.adrianhescalante.photos500.network.dtos;

import com.google.gson.annotations.SerializedName;

public class PhotoDto {
    @SerializedName("id")
    public int id;
    @SerializedName("description")
    public String description;
    @SerializedName("image_url")
    public String imageUrl;
    @SerializedName("user")
    public UserDto user;
}
