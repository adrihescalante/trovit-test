package com.adrianhescalante.photos500.network;

import com.adrianhescalante.photos500.network.dtos.SearchPhotosDto;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface Photos500Api {
    @GET("v1/photos/search")
    Call<SearchPhotosDto> searchUsers(@Query("term") String query);
}
