package com.adrianhescalante.photos500.network;

import android.support.annotation.NonNull;

import com.adrianhescalante.common.network.BaseClient;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Photos500Client implements BaseClient<Photos500Api> {
    private static final String BASE_URL = "https://api.500px.com/";
    private static final String CONSUMER_KEY = "K0nMK4dOqd4KZYPKsNy6VlmgVPPlhMGxeGVp0Eth";

    private Photos500Api client;

    public Photos500Client() {
        OkHttpClient.Builder httpClientBuilder = new OkHttpClient.Builder();
        httpClientBuilder.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(@NonNull Chain chain) throws IOException {
                Request original = chain.request();
                HttpUrl originalHttpUrl = original.url();

                HttpUrl url = originalHttpUrl.newBuilder()
                        .addQueryParameter("consumer_key", CONSUMER_KEY)
                        .build();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .url(url);

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(httpClientBuilder.build())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        client = retrofit.create(Photos500Api.class);
    }

    @Override
    public Photos500Api getApi() {
        return client;
    }
}
