package com.adrianhescalante.photos500.network.dtos;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SearchPhotosDto {
    @SerializedName("photos")
    public List<PhotoDto> photos;
}
