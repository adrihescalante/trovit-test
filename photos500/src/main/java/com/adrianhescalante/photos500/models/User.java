package com.adrianhescalante.photos500.models;

import com.adrianhescalante.photos500.network.dtos.UserDto;

import java.io.Serializable;

public class User implements Serializable {
    private int id;
    private String username;

    public int getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public static User fromDto(UserDto dto) {
        User user = new User();

        user.id = dto.id;
        user.username = dto.username;

        return user;
    }

    public String getProfileUrl() {
        return "https://500px.com/" + username;
    }
}
