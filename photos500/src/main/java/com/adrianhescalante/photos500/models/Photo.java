package com.adrianhescalante.photos500.models;

import com.adrianhescalante.common.models.BaseItemModel;
import com.adrianhescalante.common.ui.detail.DetailMvp;
import com.adrianhescalante.common.ui.search.SearchMvp;
import com.adrianhescalante.photos500.network.dtos.PhotoDto;
import com.adrianhescalante.photos500.network.dtos.SearchPhotosDto;

import java.util.ArrayList;
import java.util.List;

public class Photo extends BaseItemModel implements SearchMvp.Model, DetailMvp.Model {
    private int id;
    private String description;
    private String imageUrl;
    private User user;

    public Photo(int id) {
        super(id);

        this.id = id;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public String getTitle() {
        return description;
    }

    @Override
    public String getImageUrl() {
        return imageUrl;
    }

    @Override
    public String getProfileUrl() {
        return user.getProfileUrl();
    }

    public String getDescription() {
        return description;
    }

    public User getUser() {
        return user;
    }

    public static Photo fromPhotoDto(PhotoDto dto) {
        Photo photo = new Photo(dto.id);

        photo.description = dto.description;
        photo.imageUrl = dto.imageUrl;
        photo.user = User.fromDto(dto.user);

        return photo;
    }

    public static List<Photo> fromSearchPhotosDto(SearchPhotosDto dtos) {
        List<Photo> users = new ArrayList<>();

        for (PhotoDto dto : dtos.photos) {
            users.add(Photo.fromPhotoDto(dto));
        }

        return users;
    }
}
