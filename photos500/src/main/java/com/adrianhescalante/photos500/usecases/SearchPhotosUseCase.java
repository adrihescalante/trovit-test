package com.adrianhescalante.photos500.usecases;

import com.adrianhescalante.common.database.AppDatabase;
import com.adrianhescalante.common.models.BaseItemModel;
import com.adrianhescalante.common.network.BaseClient;
import com.adrianhescalante.common.usecases.BaseUseCase;
import com.adrianhescalante.common.usecases.Executor;
import com.adrianhescalante.common.usecases.OnResponseListener;
import com.adrianhescalante.common.usecases.ResponseNotifier;
import com.adrianhescalante.common.usecases.visited.VisitedUseCase;
import com.adrianhescalante.photos500.models.Photo;
import com.adrianhescalante.photos500.network.Photos500Api;
import com.adrianhescalante.photos500.network.dtos.SearchPhotosDto;

import java.io.IOException;
import java.util.List;

import retrofit2.Response;

public class SearchPhotosUseCase extends BaseUseCase {
    private BaseClient<Photos500Api> client;
    private VisitedUseCase visitedUseCase;

    public SearchPhotosUseCase(Executor executor, BaseClient<Photos500Api> client, AppDatabase database) {
        super(executor);

        this.visitedUseCase = new VisitedUseCase(executor, database);
        this.client = client;
    }

    public void searchPhotos(final String query, final OnResponseListener<List<Photo>> listener) {
        executor.runOnBackground(new Runnable() {
            @Override
            public void run() {
                ResponseNotifier<List<Photo>> notifier = new ResponseNotifier<>(executor, listener);

                try {
                    Response<SearchPhotosDto> response = client.getApi().searchUsers(query).execute();
                    if (response.isSuccessful()) {
                        List<Photo> photos = Photo.fromSearchPhotosDto(response.body());

                        List<BaseItemModel> baseItemModels = visitedUseCase.getAllVisitedSync();
                        notifier.notifySuccess(visitedUseCase.mergeItems(baseItemModels, photos));
                    } else {
                        notifier.notifyError(response.message());
                    }
                } catch (IOException exception) {
                    notifier.notifyError(exception);
                }
            }
        });
    }
}
